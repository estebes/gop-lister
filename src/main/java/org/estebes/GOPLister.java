package org.estebes;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.commons.io.FileUtils;
import org.estebes.gui.MainGui;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by estebes on 19-04-2017.
 * Project: goplister
 */
public class GOPLister extends Application {
    public static void main(String[] args) {
        System.setProperty("http.agent", "Chrome");
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        final GridPane gridPane = new MainGui();
        final ListView<Track> listView = new ListView<Track>();
        listView.setEditable(false);
        AnchorPane.setTopAnchor(listView, 0.0D);
        AnchorPane.setLeftAnchor(listView, 0.0D);
        AnchorPane.setBottomAnchor(listView, 0.0D);
        AnchorPane.setRightAnchor(listView, 0.0D);

        listView.setCellFactory(new Callback<ListView<Track>, ListCell<Track>>() {
            @Override
            public ListCell<Track> call(ListView<Track> param) {
                return new ListCell<Track>() {
                    @Override
                    protected void updateItem(Track track, boolean empty) {
                        super.updateItem(track, empty);

                        if (empty || track == null || track.getName() == null) {
                            setText(null);
                        } else {
                            setText(track.getName());
                        }
                    }
                };
            }
        });
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    try {
                        Track track = listView.getSelectionModel().getSelectedItem();
                        //GOPLister.this.getHostServices().showDocument("http://history.radiorecord.ru/air/gop/2017-04-19/" +
                                //track.getLink());
                        String name = track.getName();
                        name = name.replace(":", "_");
                        name += ".mp3";
                        if(!Files.exists(Paths.get("D:/Gop/" + name))) {
                            System.out.println("File doesnt exist");
                            FileUtils.copyURLToFile(new URL("http://history.radiorecord.ru/air/gop/" + DateUtil.getMoscowDate() + "/" +
                                    track.getLink()), new File("D:/Gop/", name), 5000, 5000);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        });

        try {
            Document page = Jsoup.connect("http://history.radiorecord.ru/air/gop/" + DateUtil.getMoscowDate() + "/").get();
            Elements links = page.select("a[href]");
            for(Element element : links) {
                String link = element.attr("href");
                String name = URLDecoder.decode(link, "UTF-8");
                if(!name.contains(" -  - .mp3") && !name.contains("GOP FM")) {
                    name = name.replace(".mp3", "");
                    //System.out.println(name);
                    listView.getItems().addAll(new Track(name, link));
                    listView.scrollTo(listView.getItems().size() - 1);
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        stage.setScene(new Scene(gridPane, 1280.0D, 720.0D));
        stage.setTitle("GOP Lister");
        stage.show();
    }

    class Track {
        public Track(String name, String link) {
            this.name = name;
            this.link = link;
        }

        public String getName() {
            return name;
        }

        public String getLink() {
            return link;
        }

        private final String name, link;
    }
}
