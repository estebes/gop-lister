package org.estebes.gui;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * Created by estebes on 21-04-2017.
 * Project: goplister
 */
public class MainGui extends GridPane {
    public MainGui() {
        super();

        ColumnConstraints sidebar = new ColumnConstraints();
        sidebar.setPercentWidth(20);
        ColumnConstraints body = new ColumnConstraints();
        body.setPercentWidth(80);
        this.getColumnConstraints().addAll(sidebar, body);

        RowConstraints row = new RowConstraints();
        row.setVgrow(Priority.ALWAYS);
        this.getRowConstraints().addAll(row);

        channelList = new ChannelList();
        channelList.setMaxWidth(Double.MAX_VALUE);
        channelList.setMaxHeight(Double.MAX_VALUE);

        browser = new Browser();
        browserView = new BrowserView(browser);
        browser.loadURL("http://air.radiorecord.ru:805/gop_320");

        this.add(channelList, 0, 0);
        this.add(browserView, 1, 0);
    }

    public ChannelList getChannelList() {
        return channelList;
    }

    // Vars
    private final ChannelList channelList;
    private final Browser browser;
    private final BrowserView browserView;
}
