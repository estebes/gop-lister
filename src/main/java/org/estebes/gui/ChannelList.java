package org.estebes.gui;

import com.jfoenix.controls.JFXListView;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.estebes.RecordChannel;

/**
 * Created by estebes on 21-04-2017.
 * Project: goplister
 */
public class ChannelList extends AnchorPane {
    public ChannelList() {
        super();

        channelList = new JFXListView<Channel>();
        //channelList.setMaxWidth(Double.MAX_VALUE);
        //channelList.setMaxHeight(Double.MAX_VALUE);
        AnchorPane.setTopAnchor(channelList, 0.0D);
        AnchorPane.setLeftAnchor(channelList, 0.0D);
        AnchorPane.setBottomAnchor(channelList, 0.0D);
        AnchorPane.setRightAnchor(channelList, 0.0D);

        channelList.setCellFactory(new Callback<ListView<Channel>, ListCell<Channel>>() {
            @Override
            public ListCell<Channel> call(ListView<Channel> param) {
                return new ListCell<Channel>() {
                    @Override
                    protected void updateItem(Channel channel, boolean empty) {
                        super.updateItem(channel, empty);

                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem download = new MenuItem("Download");
                        MenuItem open = new MenuItem("Open in browser");
                        contextMenu.getItems().addAll(download, open);

                        if (empty || channel == null || channel.getName() == null) {
                            setText(null);
                        } else {
                            setText(channel.getName());
                            setContextMenu(contextMenu);
                        }
                    }
                };
            }
        });

        for(RecordChannel recordChannel : RecordChannel.values()) {
            channelList.getItems().addAll(new Channel(recordChannel));
            channelList.scrollTo(channelList.getItems().size() - 1);
        }

        this.getChildren().addAll(channelList);
    }

    public JFXListView<Channel> getChannelList() {
        return channelList;
    }

    // Vars
    private final JFXListView<Channel> channelList;

    private class Channel {
        public Channel(RecordChannel channel) {
            this.name = channel.getName();
            this.channel = channel;
        }

        public String getName() {
            return name;
        }

        public RecordChannel getChannel() {
            return channel;
        }

        // Vars
        private final String name;
        private final RecordChannel channel;
    }
}
