package org.estebes;

/**
 * Created by estebes on 20-04-2017.
 * Project: goplister
 */
public enum RecordChannel {
    BLACK("Black", "yo"),
    BREAKS("Record Breaks", "brks", "Record Breaks"),
    CHILL_OUT("Record Chill-Out", "chil"),
    DANCECORE("Record Dancecore", "dc", "Dancecore"),
    DEEP("Record Deep", "deep"),
    DUBSTEP("Record Dubstep", "dub"),
    EDM("Record EDM", "club"),
    FUTURE_HOUSE("Future House", "fut", "Record Future House"),
    GOA_PSY("GOA/PSY", "goa"),
    GOLD("Gold", "gold"),
    GOP("Гоп FM", "gop", "GOP FM"),
    HARDSTYLE("Record Hardstyle", "teo"),
    MDL("Медляк FM", "mdl"),
    MEGAMIX("Record Megamix", "mix"),
    MINIMAL_TECH("Minimal/Tech", "mini", "Record Minimal"),
    NAFT("Нафталин FM", "naft"),
    OLD_SCHOOL("Old School", "pump", "Record Old School"),
    PIRATE_STATION("Pirate Station", "ps"),
    RADIO_RECORD("Radio Record", "rr", "Record Dance Radio"),
    RAVE_FM("Rave FM", "rave", "Rave FM"),
    ROCK("Record Rock", "rock"),
    RUSSIAN_MIX("Russian Mix", "rus"),
    SD90("Супердискотека 90-х", "sd90"),
    TRANCEMISSION("Trancemission Radio", "tm"),
    TRAP("Record Trap", "trap"),
    TROPICAL("Tropical", "trop"),
    VIP_HOUSE("Vip House", "vip", "VIP");

    private RecordChannel(String name, String dir) {
        this(name, dir, "");
    }

    private RecordChannel(String name, String dir, String fx) {
        this.name = name;
        this.dir = dir;
        this.fx = fx;
    }

    public String getName() {
        return name;
    }

    public String getDir() {
        return dir;
    }

    public String getFx() {
        return fx;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("Name: ");
        ret.append(name);
        ret.append(" | Dir: ");
        ret.append(dir);
        return ret.toString();
    }

    // Vars
    private final String name;
    private final String dir;
    private final String fx;
}
