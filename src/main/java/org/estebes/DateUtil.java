package org.estebes;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by estebes on 20-04-2017.
 * Project: goplister
 */
public class DateUtil {
    public static void main(String[] args) {
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime currentTime = LocalDateTime.now();
        ZonedDateTime moscowTime = ZonedDateTime.now(ZoneId.of("Europe/Moscow"));
        System.out.println(currentTime.format(formatter));
        System.out.println(moscowTime.format(formatter));
    }

    public static String getMoscowDate() {
        ZonedDateTime moscowTime = ZonedDateTime.now(ZoneId.of("Europe/Moscow"));
        return moscowTime.format(formatter);
    }

    // Vars
    static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
}
